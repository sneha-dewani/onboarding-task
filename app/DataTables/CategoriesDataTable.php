<?php

namespace App\DataTables;

use App\Features\Categories\Domain\Models\Category;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CategoriesDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('is_active', function ($category){
                $badgeColor = $category->is_active ? "success" : "danger";
                $text = $category->is_active ? "Active" : "Inactive";
                return "<span class = 'badge badge-{$badgeColor}'> {$text} </span>";
            })
            ->addColumn('created_at', function($category){
                return $category->created_at->format('d-m-Y');
            })
            ->addColumn('updated_at', function($category){
                return $category->updated_at->format('d-m-Y');
            })
            ->addColumn('edit', function($category){
                return '<a href="' . route('admin.categories.edit', $category->id) . '">' .'Edit</a>';
            })
            ->addColumn('delete', function($category){
                return '<a href="' . route('admin.categories.destroy', $category->id) . '">' .'Delete</a>';
            })

            ->rawColumns(['is_active','created_at','updated_at', 'edit','delete']);
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Category $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('categories-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->parameters([
                        'lengthMenu' => [
                            [25, 50, 100, 200],
                            ["25 rows", "50 rows", "100 rows", "200 rows"],
                        ],
                        "order" => [
                            [0, "DESC"]
                        ]
                    ]);

    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id'),
            Column::make('name'),
            Column::make('description'),
            Column::make('is_active')->title('Status'),
            Column::make('created_at')->title('Created At'),
            Column::make('updated_at'),
            Column::make('edit')->title('Edit'),
            Column::make('delete')
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Categories_' . date('YmdHis');
    }
}
