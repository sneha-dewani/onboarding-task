<?php

namespace App\Features\Categories\Domain\Models;
use App\Features\Categories\Domain\Models\Constants\CategoryConstants;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions\StudentT;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends BaseModel implements CategoryConstants
{
    protected $table = 'categories';

    public static function persistCategory(array $data): self
    {
        Utils::validateOrThrow($data, self::persistRules());
        return DB::transaction(function () use ($data){
            return Category::create($data);
        });
    }

    public function updateCategory(array $data): Category
    {
        Utils::validateOrThrow($data, self::updateRules());
        DB::transaction(function () use ($data){
            $this->update($data);
            // dd($data);
        });
         return $this;
    }

    public function deleteCategory(){
        DB::transaction(function (){
            $this->delete();
        });
        return $this;
    }

    public static function persistRules() :array{
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules():array
    {
        return self::UPDATE_VALIDATION_RULES;
    }
}
