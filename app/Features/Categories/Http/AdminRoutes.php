<?php
use Illuminate\Support\Facades\Route;

Route::resource(
    '/categories',
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class]
);

Route::post("categories/get-categories",
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class]
)->name('admin.categories.get-categories');
