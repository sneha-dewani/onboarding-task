<?php

namespace App\Features\Categories\Http\Controllers\Admin\V1\Controllers;

use App\DataTables\CategoriesDataTable;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Categories\Http\Controllers\Admin\V1\Actions\CategoriesAction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoriesController extends Controller
{
    public function __construct(private CategoriesAction $categoriesAction){}
    public function index(CategoriesDataTable $dataTable)
    {
        return $dataTable->render('admin.categories.index');
    }

    public function create(Request $request)
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        try{
            $data['name'] = $request->name;
            $data['description'] = $request->description;
            $data['is_active'] = $request->state === "1";
            $this->categoriesAction->persistCategory($data);
            session()->flash("success", "Category details saved succesfully!");
        } catch(\Exception $execption){
            Log::error($execption->getMessage());
            session()->flash("error", "Category details failed to save");
        }
        return redirect()->route('admin.categories.index');
    }

    public function edit(Category $category, Request $request)
    {
        return view('admin.categories.edit', compact(["category"]));
    }

    public function update(Request $request, Category $category){
        try{
            $data['name'] = $request->name;
            $data['description'] = $request->description;
            $data['is_active'] = $request->state === "1";
            $this->categoriesAction->updateCategory($category, $data);
            session()->flash("success", "Category details updated succesfully!");
        }catch(\Exception $execption){
            Log::error($execption->getMessage());
            session()->flash("error", "Category details failed to update");
        }
        return redirect()->route('admin.categories.index');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        session()->flash("success", "Category details deleted succesfully!");
        return redirect()->route('admin.categories.index');
    }
}
