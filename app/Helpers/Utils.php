<?php
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class Utils{
    public static function validateOrThrow(array $data, array $validaionRules, array $validationRulesMessages = []) : array {
        $validator = Validator::make($data, $validaionRules, $validationRulesMessages);
        if($validator->fails()){
            throw ValidationException::withMessages($validator->getMessageBag()->toArray());
        }
        return $validator->validated();
    }
}
