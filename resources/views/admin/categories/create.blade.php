@extends('admin.layouts.app')

@section('header')
    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center">
                <div class="col">
                    <!-- Page pre-title -->
                    <h2 class="page-title">
                        Create Category
                    </h2>
                </div>

            </div>
        </div>
    </div>

@endsection


@section('main-content')
    <div class="container-xl">
    <div class="row">
        <div class="col-md-12">
            <form action="{{route("admin.categories.store")}}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-12 mb-3">
                        <label for="first_name">Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Electronics">
                    </div>
                    <div class="form-group col-md-12 mb-3">
                        <label for="last_name">Description</label>
                        <textarea type="text" class="form-control" name="description" id="description" placeholder="Some short description of category"></textarea>
                    </div>
                    <div class="form-group col-md-12 mb-3">
                        <label for="">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12 d-flex justify-content-start mb-3">
                        <input type="submit" class="btn btn-primary" value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
@endsection

