<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

//Route::resource(
//    '/categories',
//    \App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class
//);

Route::get(
    '/categories',
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'index']
)->name('admin.categories.index');

Route::get(
    '/categories/create',
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'create']
)->name('admin.categories.create');

Route::post(
    '/categories/store',
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'store']
)->name('admin.categories.store');

Route::get(
    '/categories/{category}/edit',
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'edit']
)->name('admin.categories.edit');

Route::put(
    '/categories/{category}',
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'update']
)->name('admin.categories.update');


Route::get(
    '/categories/{category}/delete',
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'destroy']
)->name('admin.categories.destroy');
